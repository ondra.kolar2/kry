from models import *


def write_to_database(result, scan_id):
    try:
        # Iterace přes všechny hostitele ve výsledku skenování
        for host in result.all_hosts():
            # Získání IPv4 adresy hostitele, výchozí hodnota je 'Unknown'
            ip_address = result[host]['addresses'].get('ipv4', 'Unknown')
            # Získání hostname, výchozí hodnota je 'Unknown'
            hostname = result[host]['hostnames'][0]['name'] if result[host]['hostnames'] else 'Unknown'
            # Získání typu zařízení, výchozí hodnota je 'Unknown'
            device_type = result[host]['hostnames'][0]['type'] if result[host]['hostnames'] else 'Unknown'

            # Vytvoření nového zařízení v databázi
            new_device = Device(
                ip_address=ip_address,
                hostname=hostname,
                device_type=device_type,
                scan_id=scan_id
            )
            db.session.add(new_device)
            db.session.flush()  # Uložení změn pro získání ID nového zařízení

            # Zpracování informací o operačním systému
            for os_match in result[host].get('osmatch', []):
                new_os = OperationSystem(
                    name=os_match['name'],
                    accuracy=os_match['accuracy'],
                    device_id=new_device.id
                )
                db.session.add(new_os)

            # Zpracování informací o portech
            for protocol in result[host].all_protocols():
                for port in result[host][protocol].keys():
                    service = result[host][protocol][port]
                    if service['state'] == 'open':
                        new_port = Port(
                            number=port,
                            state=service['state'],
                            service_name=service.get('name', 'Unknown'),
                            product=service.get('product', 'Unknown'),
                            version=service.get('version', 'Unknown'),
                            extra_info=service.get('extrainfo', 'Unknown'),
                            device_id=new_device.id
                        )
                        db.session.add(new_port)

        # Potvrzení všech změn po přidání všech záznamů
        db.session.commit()
    except Exception as e:
        # V případě chyby během procesu provede rollback
        db.session.rollback()
        print(f"Nastala chyba: {e}")
    finally:
        # Uzavření session bez ohledu na to, zda došlo k chybě
        db.session.close()


def write_to_database_zgrab(result, scan_id):
    # Získání IP adresy ze skenování
    ip_address = result.get('ip', None)
    if ip_address is not None:
        # Vyhledání zařízení v databázi podle IP adresy a ID skenování
        device = Device.query.filter_by(ip_address=ip_address, scan_id=scan_id).first()
        if device is not None:
            # Získání dat specifických pro Siemens
            siemens_data = result.get('data', {}).get('siemens', {})
            zgrab_entry = Zgrab(
                # Přiřazení dat ze skenování do nového záznamu
                status=siemens_data.get('status', 'Unknown'),
                protocol=siemens_data.get('protocol', 'Unknown'),
                is_s7=siemens_data.get('result', {}).get('is_s7', 'Unknown'),
                system=siemens_data.get('result', {}).get('system', 'Unknown'),
                module=siemens_data.get('result', {}).get('module', 'Unknown'),
                plant_id=siemens_data.get('result', {}).get('plant_id', 'Unknown'),
                copyright=siemens_data.get('result', {}).get('copyright', 'Unknown'),
                serial_number=siemens_data.get('result', {}).get('serial_number', 'Unknown'),
                module_type=siemens_data.get('result', {}).get('module_type', 'Unknown'),
                reserved_for_os=siemens_data.get('result', {}).get('reserved_for_os', 'Unknown'),
                location=siemens_data.get('result', {}).get('location', 'Unknown'),
                module_id=siemens_data.get('result', {}).get('module_id', 'Unknown'),
                hardware=siemens_data.get('result', {}).get('hardware', 'Unknown'),
                firmware=siemens_data.get('result', {}).get('firmware', 'Unknown'),
                timestamp=datetime.strptime(siemens_data.get('timestamp', 'Unknown'),
                                            "%Y-%m-%dT%H:%M:%S%z") if siemens_data.get('timestamp') else 'Unknown',
                device_id=device.id
            )
            db.session.add(zgrab_entry)
            db.session.commit()  # Uložení změn do databáze
            return zgrab_entry.id
    else:
        return None
