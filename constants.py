import os

# Název souboru pro uložení výsledků nástroje ZGRAB
FILE_NAME = "hosts_up.txt"

# Získání absolutní cesty k adresáři, ve kterém se nachází tento skript
BASEDIR = os.path.abspath(os.path.dirname(__file__))

# Konstrukce cesty k souboru 'hosts_up.txt' v aktuálním adresáři
FILE_PATH = os.path.join(BASEDIR, FILE_NAME)

# Konfigurační řetězec pro připojení k SQLite databázi, umístěné v adresáři projektu
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASEDIR, 'mydatabase.db')

# Nastavení SQLAlchemy, aby nehlídalo změny na objektech, pro výkonnější chod
SQLALCHEMY_TRACK_MODIFICATIONS = False
