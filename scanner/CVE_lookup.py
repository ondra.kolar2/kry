from pycvesearch import CVESearch
from googlesearch import search
import requests
from bs4 import BeautifulSoup
import re
from models import *


def zgrab_cve_vuln(result, zgrab_id):
    # Získání informací ze skenování zařízení Siemens
    siemens_data = result.get('data', {}).get('siemens', {})
    # Pokus o získání systémového jména, jinak zkusíme získat typ modulu
    query = siemens_data.get('result', {}).get('system', 'Unknown'),

    if query == 'Unknown':
        query = siemens_data.get('result', {}).get('module_type'),

    # Přidání "vulnerability" k dotazu pro zvýšení relevance výsledků
    query = str(query[0]) + " vulnerability"

    # Seznam pro ukládání výsledků vyhledávání
    results = []
    print("Spouštím hledání")
    # Provedení vyhledávání v Google
    try:
        for url in search(query, num_results=10):
            results.append(url)
    except Exception as e:
        print(f"Error: {e}")

    print(results)

    if len(results) == 0:
        return

    # Regulární výraz pro vyhledávání identifikátorů CVE
    cve_pattern = re.compile(r"CVE-\d{4}-\d{5}")

    # Slovník pro ukládání jedinečných shod a jejich skóre
    unique_matches = {}

    print("Spouštím hledání matchů")
    for url in results:
        try:
            response = requests.get(url)
        except requests.exceptions.ConnectionError:
            print(f"Failed to establish a connection to {url}. Skipping this URL.")
            continue
        soup = BeautifulSoup(response.text, 'html.parser')
        matches = cve_pattern.findall(soup.text)
        if matches:
            # Přidáme pouze první dvě shody do slovníku a inkrementujeme jejich skóre
            for match in matches[:2]:
                if match in unique_matches:
                    unique_matches[match] += 1
                else:
                    unique_matches[match] = 0

        # Seřazení slovníku podle skóre, sestupně
        unique_matches = dict(sorted(unique_matches.items(), key=lambda item: item[1], reverse=True))
        print(unique_matches)

    print("Hledání CVE v databázi")
    # Inicializace CVESearch s URL databáze CVE
    cve = CVESearch("https://cve.circl.lu/")

    for cve_id, score in unique_matches.items():
        print(type(cve.id(cve_id)))
        data = cve.id(cve_id)

        if data is not None:
            # Vytvoření nové instance Vulnerability
            vulnerability = ZgrabVuln(
                name=data['id'],  # cve_id
                score=float(data['cvss']) if data['cvss'] else None,
                url=data['references'][0] if data['references'] else None,  # první odkaz z "references"
                match=score,  # skóre z unique_matches
                zgrab_id=zgrab_id  # zgrab_id musí být poskytnut
            )

            # Vložení do databáze
            db.session.add(vulnerability)
            db.session.commit()
