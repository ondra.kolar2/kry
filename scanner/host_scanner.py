import concurrent.futures
from constants import *
from scanner.vulnerability_scanner import *
from scanner.nmap_scanner import run_nmap_scan
from scanner.zgrab_scanner import run_zgrab
from database.db_operations import write_to_database


def initialize_hosts(ip_address, port_range, scan_name, vuln_scan_checkbox, thread_num):
    # Inicializace nmap a skenování dostupných (UP) hostů pomocí PING
    nm = nmap.PortScanner()
    nm.scan(hosts=ip_address, arguments='-n -sP')
    hosts_list = [(x, nm[x]['status']['state']) for x in nm.all_hosts()]

    # Funkce pro řazení IP adres podle posledního oktetu
    def sort_by_last_octet(ip_address):
        return int(ip_address.split('.')[-1])

    # Filtrace a seřazení aktivních IP adres
    ip_addresses = [ip for ip, status in hosts_list if status == 'up']
    ip_addresses = sorted(ip_addresses, key=sort_by_last_octet)

    # Uložení aktivních IP adres do souboru pro další zpracování pomocí Zgrab
    with open(FILE_PATH, 'w') as file:
        for ip in ip_addresses:
            file.write(ip + '\n')
    file.close()

    # Registrace nového skenování v databázi
    if len(ip_addresses) == 0:
        return None
    else:
        scan_id = initialize_scan_db(scan_name)

    # Paralelní spuštění Nmap skenů s využitím více vláken
    with concurrent.futures.ThreadPoolExecutor(max_workers=int(thread_num)) as executor:
        unique_ip_addresses = set(ip_addresses)
        futures = {executor.submit(run_nmap_scan, ip, port_range, vuln_scan_checkbox) for ip in unique_ip_addresses}

        # Ukládání výsledků do databáze a spuštění skenování zranitelností (CVE)
        for future in concurrent.futures.as_completed(futures):
            scanner = future.result()
            write_to_database(scanner, scan_id)
            if vuln_scan_checkbox == 'on':
                CVE_scan(scanner, 0, 0, scan_id)

    # Spuštění Zgrab pro další analyzování síťového provozu
    run_zgrab(scan_id)
    return 0

def initialize_scan_db(name):
    # Vytvoření záznamu o skenování s aktuálním časovým razítkem
    timestamp = datetime.utcnow()
    new_scan = Scan(timestamp=timestamp, name=name)
    db.session.add(new_scan)
    db.session.flush()  # Získání ID skenování pro použití v cizích klíčích
    return new_scan.id
