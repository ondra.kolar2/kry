import nmap

def run_nmap_scan(upHost, port_range, vuln_scan_checkbox):
    # Vytvoření instance skeneru nmap pro detailní analýzu
    detailed_scanner = nmap.PortScanner()

    # Nastavení argumentů pro nmap: provádí sken portů a zjišťuje verze služeb a operační systém
    # Pokud je zvolena kontrola zranitelností, přidá se skript 'vulners'
    if vuln_scan_checkbox == 'on':
        detailed_options = f"-p {port_range} -sV -O --script vulners"  # Rozšířené skenování s kontrolou zranitelností
    else:
        detailed_options = f"-p {port_range} -sV -O"  # Základní skenování bez kontroly zranitelností

    # Spuštění nmap s definovanými argumenty na specifikovaných hostech
    # 'upHost' je seznam hostů, kteří jsou dostupní a spojuje se do jednoho řetězce odděleného mezerami
    detailed_scanner.scan(hosts=upHost, arguments=detailed_options)

    # Vrací instanci skeneru pro další zpracování výsledků
    return detailed_scanner