from database.db_operations import *
import subprocess
import json
from scanner.CVE_lookup import *


def run_zgrab(scan_id):
    # Spustí zgrab2 skript s konfigurací pro Siemens zařízení, používá soubor s aktivními hosty
    result = subprocess.run(['./zgrab2', 'siemens', '-f', 'hosts_up.txt'], capture_output=True, text=True)

    # Kontrola, zda příkaz proběhl úspěšně
    if result.returncode != 0:
        print('Příkaz zgrab2 selhal')
        return None

    # Převod výstupu příkazu na JSON objekty
    json_results = [json.loads(line) for line in result.stdout.splitlines()]

    # Procházení výsledků a zpracování informací o zařízeních
    for result in json_results:
        status = result.get('data', {}).get('siemens', {}).get('status')
        if status == 'connection-timeout':
            # V případě timeoutu připojení, nic se neděje
            pass
        elif status == 'success':
            # U úspěšných výsledků se informace zapisují do databáze
            zgrab_id = write_to_database_zgrab(result, scan_id)
            zgrab_cve_vuln(result, zgrab_id)

    # Vrací výsledek pro možné další zpracování
    return result
