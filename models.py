from datetime import datetime
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Scan(db.Model):
    __tablename__ = 'scans'
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)  # Časové razítko vytvoření záznamu
    name = db.Column(db.String, nullable=False)  # Název skenování
    devices = db.relationship('Device', backref='scan', lazy=True)  # Seznam zařízení ve skenování


class Device(db.Model):
    __tablename__ = 'devices'
    id = db.Column(db.Integer, primary_key=True)
    ip_address = db.Column(db.String, index=True, nullable=False)  # IP adresa zařízení
    device_type = db.Column(db.String, nullable=True)  # Typ zařízení (obecný účel, tiskárna, atd.)
    hostname = db.Column(db.String, nullable=True)  # Hostitelské jméno zařízení
    scan_id = db.Column(db.Integer, db.ForeignKey('scans.id'), nullable=False)  # Identifikátor skenování
    ports = db.relationship('Port', backref='device', lazy=True)  # Seznam portů zařízení
    operation_systems = db.relationship('OperationSystem', backref='device', lazy=True)  # Seznam operačních systémů zařízení


class OperationSystem(db.Model):
    __tablename__ = 'os'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=True)  # Název operačního systému
    accuracy = db.Column(db.String, nullable=True)  # Přesnost detekce OS
    device_id = db.Column(db.Integer, db.ForeignKey('devices.id'), nullable=False)  # Identifikátor zařízení


class Port(db.Model):
    __tablename__ = 'port'
    id = db.Column(db.Integer, primary_key=True)
    number = db.Column(db.Integer, nullable=False)  # Číslo portu
    state = db.Column(db.String, nullable=False)  # Stav portu (otevřený, uzavřený)
    service_name = db.Column(db.String, nullable=True)  # Název služby (ftp, http, atd.)
    product = db.Column(db.String, nullable=True)  # Název produktu (vsftpd, Apache httpd, atd.)
    version = db.Column(db.String, nullable=True)  # Verze služby
    extra_info = db.Column(db.String, nullable=True)  # Další informace, jsou-li dostupné
    device_id = db.Column(db.Integer, db.ForeignKey('devices.id'), nullable=False)  # Identifikátor zařízení
    vulnerabilities = db.relationship('Vulnerability', backref='port', lazy=True)  # Seznam zranitelností na portu


class Vulnerability(db.Model):
    __tablename__ = 'vulnerabilities'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)  # Název zranitelnosti
    score = db.Column(db.Float, nullable=False)  # Skóre zranitelnosti
    url = db.Column(db.String)  # URL s detailními informacemi o zranitelnosti
    exploit = db.Column(db.Boolean, nullable=True)  # Dostupnost exploitu
    port_id = db.Column(db.Integer, db.ForeignKey('port.id'), nullable=False)  # Identifikátor portu


class Zgrab(db.Model):
    __tablename__ = 'zgrab'
    id = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.String, nullable=False)  # Stav připojení
    protocol = db.Column(db.String, nullable=False)  # Protokol
    is_s7 = db.Column(db.Boolean, nullable=False)  # Indikátor protokolu S7
    system = db.Column(db.String, nullable=True)  # Systémové informace
    module = db.Column(db.String, nullable=True)  # Modul
    plant_id = db.Column(db.String, nullable=True)  # Identifikátor závodu
    copyright = db.Column(db.String, nullable=True)  # Autorské právo
    serial_number = db.Column(db.String, nullable=True)  # Sériové číslo
    module_type = db.Column(db.String, nullable=True)  # Typ modulu
    reserved_for_os = db.Column(db.String, nullable=True)  # Rezervováno pro OS
    location = db.Column(db.String, nullable=True)  # Umístění
    module_id = db.Column(db.String, nullable=True)  # ID modulu
    hardware = db.Column(db.String, nullable=True)  # Hardware
    firmware = db.Column(db.String, nullable=True)  # Firmware
    timestamp = db.Column(db.DateTime, nullable=False)  # Časové razítko záznamu
    device_id = db.Column(db.Integer, db.ForeignKey('devices.id'), nullable=False)  # Identifikátor zařízení


class ZgrabVuln(db.Model):
    __tablename__ = 'zgrabvuln'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)  # Název zranitelnosti
    score = db.Column(db.Float, nullable=True)  # Skóre zranitelnosti
    url = db.Column(db.String)  # URL s detailními informacemi o zranitelnosti
    match = db.Column(db.Integer)
    zgrab_id = db.Column(db.Integer, db.ForeignKey('zgrab.id'), nullable=False)  # Identifikátor záznamu zgrab
