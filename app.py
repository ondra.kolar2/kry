from flask import Flask, redirect, url_for, abort
from models import *
from flask_migrate import Migrate
from constants import *
from flask import render_template, request
from scanner.host_scanner import initialize_hosts

app = Flask(__name__)

# Nastavení konfigurace pro připojení k databázi SQLite
app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = SQLALCHEMY_TRACK_MODIFICATIONS

# Inicializace SQLAlchemy a Migrate pro správu databázových migrací
db.init_app(app)
migrate = Migrate(app, db)

# Automatické vytvoření databázových tabulek na základě definovaných modelů
with app.app_context():
    db.create_all()


@app.route('/')
def index():
    # Zobrazení hlavní stránky aplikace
    return render_template('index.html')


@app.route('/recent')
def recent():
    # Načtení a zobrazení posledních provedených skenů z databáze
    scans = Scan.query.all()
    return render_template('recent.html', scans=scans)


@app.route('/device/<id>')
def device(id):
    # Zobrazení detailů specifického zařízení pomocí jeho ID
    device = Device.query.get(id)
    if device is None:
        abort(404)  # Device not found
    zgrab_data = Zgrab.query.filter_by(device_id=id).first()
    if zgrab_data is not None:
        zgrab_vuln = ZgrabVuln.query.filter_by(zgrab_id=zgrab_data.id).all()
    else:
        zgrab_vuln = None
    print(zgrab_vuln)
    return render_template('device.html', device=device, zgrab_data=zgrab_data, zgrab_vuln=zgrab_vuln)


@app.route('/scan', methods=['POST'])
def scan():
    # Spuštění skenování sítě na základě uživatelských vstupů z formuláře
    target = request.form['ip_address']
    ports = request.form['port_range']
    scan_name = request.form['scan_name']
    thread_num = request.form['thread_num']
    vuln_scan_checkbox = request.form.get('vuln_scan')

    initialize_hosts(target, ports, scan_name, vuln_scan_checkbox, thread_num)

    return redirect(url_for('recent'))


@app.route('/port/<id>')
def port(id):
    # Zobrazení informací o specifickém portu
    port = Port.query.get(id)
    if port is None:
        abort(404)

    return render_template('port.html', port=port)


@app.template_filter('join_ports')
def join_ports(ports):
    # Filtrovací funkce pro převod seznamu portů na čárkou oddělený řetězec
    return ', '.join(str(port.number) for port in ports)


if __name__ == '__main__':
    # Spuštění aplikace v režimu pro ladění
    app.run(debug=True)
