
# Advance network scanner

Tento projekt se zabývá tématem skenování průmyslových zranitelností v sítích. Jedná se o webovou aplikaci v jazyce Python. Prvotní sken je založen na nástroji Nmap. Aktivní zařízení nalezené pomocí skenu jsou zapsané do textového souboru "hosts_up.txt" a následně je spuštěn další sken pomocí nástroje Zgrab2. Tento sken cílí na zařízení Siemens. Pomocí výsledků skenů jsou následně hledané zranitelnosti v CVE (Common Vulnerabilities and Exposures) databázích. Výsledky skenů jsou uložené ve vytvořené databázi. 


## Skenování sítí
Skenování sítě je proces určený k identifikaci a získání informací z vyhledaných
zařízení v síti. Hlavním účelem skenu je vyhledání dostupných služeb, odhalení bezpečnostních a filtrovacích systémů, zhodnocení celkového zabezpečení nebo údržba
funkcionalit systému nebo konkrétního zařízení. Tento proces je používaný odborníky na kyberbezpečnost pro sběr informací o zařízeních, v rámci penetračního testování systémů a sítí a při identifikaci a odstranění zranitelností. Samozřejmě je
skenování používané i hackery, kteří sbírají informace o jejich cíli před samotným
útokem
## Nmap (Network Mapper)
Nmap ("Network Mapper") je vysoce flexibilní nástroj pro skenování sítí, který umožňuje objevovat zařízení a služby v počítačových sítích. Tento nástroj byl vyvinut
Gordonem Lyonem (známým též pod přezdívkou Fyodor Vaskovich) a je distribuován pod licencí GNU General Public License. Nmap je multiplatformní a podporuje
různé operační systémy, včetně Windows, Linux, a macOS.
Klíčové vlastnosti nástroje jsou skenování sítě, detekce zařízení a služeb, široké
přizpůsobení pro uživatele a podpora pro různé operační systémy.
Nmap je cenným nástrojem pro systémové administrátory, bezpečnostní odborníky a výzkumníky, kteří potřebují provádět diagnostiku sítě, identifikovat potenciální bezpečnostní hrozby a analyzovat konfigurace zařízení v síti. Jeho otevřený kód
umožňuje uživatelům přizpůsobit nástroj svým potřebám.
## Zgrab2
Zgrab2 je vysokorychlostní nástroj pro skenování a sběr informací, který byl vyvinut společností Zmap. Je navržen tak, aby umožňoval rychlé, paralelní skenování velkých rozsahů IP adres a sbírání informací o síťových službách a jejich konfiguraci. Jeho architektura umožňuje snadnou rozšiřitelnost pro podporu různých protokolů a aplikací.

Podporuje skenování různých protokolů a aplikací, což umožňuje sbírání různých typů informací o síťových službách. 

Skenování Siemens zařízení pomocí Zgrab2 je vhodné pro identifikaci a sběr informací o síťových službách provozovaných na těchto zařízeních. Siemens vyrábí širokou škálu zařízení, včetně průmyslových řídicích systémů (PLC), průmyslových komunikačních zařízení a dalších průmyslových automatizačních technologií.
## CVE
CVE (Common Vulnerabilities and Exposures) je systém označování a identifikace
unikátních bezpečnostních chyb v počítačovém software. Tato identifikace je přidělována prostřednictvím standardizovaných záznamů nazývaných "CVE ID"a je
udržována organizací MITRE Corporation.

Hlavní cíle CVE jsou identifikace a monitorování známých bezpečnostních chyb
a odhalení zranitelností v počítačových systémech a v software. Poskytuje jednotný
systém pro identifikaci a zaznamenání bezpečnostních chyb. Zranitelnostem je přidělen unikátní identifikátor "CVE-ID". Záznamy jsou uchovávány v CVE databázích,
které jsou volně dostupné a jsou využívány různými bezpečnostními organizacemi,
vývojáři a výzkumníky.

Aplikace hledá CVE zranitelnosti pomocí výsledků skenů.


## Návrh databáze
Návrh databáze se skládá ze 7 tabulek, které slouží k uchování informací z provedených skenů. 
![alt text](./dokumentace/database_diagram.png)

## Vývojový diagram
Po inicializaci aplikace, uživatel zadá parametry skenování do webového rozhraní, po odeslání je provedeno jednoduché ověření dostupnosti zařízení dle zadaného rozsahu. Následně se pro tyto zařízení spouští hloubkový nmap a zgrab sken. Nalezené informace a zranitelnosti se zapíšou do databáze a prezentují uživateli skrze webovou aplikaci.
![alt text](./dokumentace/vyvojovy_diagram.png)


## Zdrojové soubory
#### constants.py

Tento zdrojový soubor Pythonu slouží k definici cest k souborům a konfiguraci databáze pro projekt. Nejprve importuje modul os, který umožňuje pracovat s cestami k souborům a dalšími operacemi spojenými s operačním systémem. Dále definuje několik statických proměnných.

#### models.py

Tato třída definuje databázový model pro správu skenování síťových zařízení v rámci aplikace využívající framework Flask s rozšířením SQLAlchemy pro práci s relační databází.
Vytváří tabulky, které jsou v rámci skenů naplňovány. Tímto způsobem se vytváří hierarchie relačních entit pro ukládání a správu informací o síťových zařízeních a skenování. Každá entita je propojena s dalšími entitami pomocí vazeb a cizích klíčů, což umožňuje efektivní manipulaci s daty v databázi.

#### app.py

Tento soubor obsahuje funkci main a spouští aplikaci. Jedná se o aplikaci ve frameworku Flask. Nejprve se importují potřebné moduly a třídy pro funkcionalitu aplikace. Dále se vytváří instance Flask aplikace a konfigurace pro připojení k databázi SQLite. Také jsou zde definované funkce pro zobrazení jednotlivých webových stránek, včetně zpracování formulářů a spuštění skenování síťových zařízení.

### Skenner

#### host_scanner.py

Tento soubor obsahuje dvě funkce. Funkce "initialize_hosts" inicializuje skenování aktivních hostů v síti a zapisuje jejich IP adresy do souboru pro další zpracování. Nejprve se provede skenování dostupných UP hostů pomocí nástroje Nmap. Aktivní IP adresy jsou filtrovány a seřazeny. IP adresy jsou uloženy do souboru. Výsledky skenování se ukládají do databáze a případně se spouští skenování zranitelností. Nakonec se spouští Zgrab pro další analýzu síťového provozu.

Funkce "initialize_scan_db" vytváří nový záznam o skenování v databázi s aktuálním časovým razítkem a názvem skenování. Vytvořený záznam je přidán do databáze a funkce vrátí ID nově vytvořeného skenování.

#### nmap_scanner.py

V rámci tohoto souboru se nachází funkce "run_nmap_scan", která slouží k provádění skenování pomocí nástroje Nmap na specifikovaných hostech. Importuje se modul nmap, který umožňuje Pythonu komunikovat s nástrojem Nmap a provádět skenování portů a další analýzy. Tato funkce má tři parametry: upHost, port_range a vuln_scan_checkbox. upHost je seznam dostupných aktivních hostů, port_range je rozsah portů, který se má prozkoumat, a vuln_scan_checkbox indikuje, zda bude provedeno skenování zranitelností.

#### zgrab_scanner.py

Zde najdeme funkci "run_zgrab", která slouží k provádění skenování síťového provozu pomocí nástroje Zgrab2 v konkrétní konfiguraci pro zařízení Siemens. Funkce má jeden parametr scan_id, což je identifikátor skenování, který slouží k asociovaní výsledků skenování s konkrétním skenováním v databázi. Dále se spouští sken s konfigurací pro skenování Siemens zařízení a souborem s aktivními hosty. Výstup příkazu se zpracovává jako JSON objekty. Pro každý výsledek se kontroluje stav připojení. Pokud je stav 'connection-timeout', nic se neděje. Pokud je stav 'success', informace o zařízení se zapisují do databáze a provádí se skenování CVE zranitelností pomocí funkce zgrab_cve_vuln. Funkce vrací výsledek pro další zpracování.

#### vulnerability_scanner.py

Tento soubor obsahuje funkci nazvanou "CVE_scan", která slouží k provádění skenování zranitelností pomocí nástroje Nmap na základě specifikovaného rozsahu IP adres a portů. Funkce má čtyři parametry: scan (instanci skeneru Nmap, pokud je již dostupná), ip_range (rozsah IP adres k prozkoumání), port_range (rozsah portů k prozkoumání) a scan_id (identifikátor skenování v databázi). Kontroluje se, zda je již dostupný sken (parametr scan). Pokud není, inicializuje se nový sken pomocí Nmap skeneru s konfigurací pro skenování zranitelností. Pro každého hosta se zpracovávají výsledky skenování zranitelností. Pro každý port se zjišťuje přítomnost zranitelností a zapisují se do databáze a do souboru.

#### CVE_lookup.py

Tento kód provádí vyhledávání CVE zranitelností na základě výsledků skenování síťového provozu pomocí nástroje Zgrab2. Obsahuje funkci "zgrab_cve_vuln", která má parametry  result (výsledek skenování síťového provozu) a zgrab_id (identifikátor skenování získaný pomocí Zgrab2). Nejprve se získává IP adresa ze skenování. Následně se vytvoří  dotaz pro vyhledání zranitelností na základě získaných informací o systému nebo typu modulu. Provede se hledání na internetu pomocí Google Search. Získané výsledky se analyzují, a pokud se v nich najdou shody s formátem CVE, přidávají se do slovníku. Pro shody se získávají detailní informace o CVE zranitelnostech a ukládají se do databáze.

## Instalace
V aktuální verzi je pouze dostupná instalace a spuštění pouze na Linuxovém zařízení. 

Stažení repozitáře
```bash
git clone https://gitlab.com/ondra.kolar2/kry.git
```

Přechod do složky a instalace potřebných knihoven
```bash 
cd kry && sudo pip install -r requirements.txt
```

## Spuštění 

Spuštění aplikace
```bash 
sudo python3 app.py 
```
Po spuštění aplikace se v terminálu zobrazí odkaz na kterém běží webová stránka, pokud výchozí port je dostupný, aplikace se spouští na adrese http://127.0.0.1:5000

## Videoukázka

![](./dokumentace/VIDEO_SCANNER_MUSIC.mp4)

## Závěr
V rámci semestrálního projektu byla vytvořena webová aplikace, sloužící ke skenování síťových zařízení a následné vyhledání jejich zranitelností. Sken dokáže identifikovat nejen běžná, ale i průmyslová zařízení a vyhledávat potenciální zranitelnosti. Výsledky jsou ukládány do SQLite databáze a skrze Python Flask webový framework jsou interpretovány uživateli. V aktuální verzi je nutné program spouštět na zařízení Linux z důvodu nekompatibility zkompilovaného zgrab2 skeneru.

